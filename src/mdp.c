#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
  if (argc != 3) {
    printf("Usage: %s <value> <base>\n", argv[0]);
    return 0;
  }

  int value = atoi(argv[1]);
  int base = atoi(argv[2]);

  int exp = base;
  int prev = value;
  int n = 1;
  int *c = malloc(sizeof(int));
  c[0] = value;

  while (exp < value) {
    int t = value / exp * exp - 1;
    if ((value + 1) % exp != 0 && prev != t) {
      n++;
      c = realloc(c, n * sizeof(int));
      c[n - 1] = t;
      prev = t;
    }
    exp *= base;
  }

  for (int i = 0; i < n; i++) {
    printf("%d ", c[i]);
  }
  printf("\n");

  free(c);

  return 0;
}
